package com.knowledegbase.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/index")
public class RenderController {

	@Value("${application.msg}")
	private String message;

	@RequestMapping("/hello")
	public ModelAndView index() {
		ModelAndView mv = new ModelAndView("hello");
		mv.addObject("hello",message);
		return mv;
	}
}
