package com.knowledegbase.controller;

import java.util.Date;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/servlet")
public class ServletController {

	@RequestMapping("/index")
	public String index() {
		return new Date() + "- index";
	}
	
	@RequestMapping("/filter1")
	public Object filter1() {
		return new Date() + " - filter1";
	}
	
	@RequestMapping("/filter2")
	public Object filter2() {
		return new Date() + " - filter2";
	}
}
