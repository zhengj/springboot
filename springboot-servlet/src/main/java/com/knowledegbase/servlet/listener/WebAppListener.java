package com.knowledegbase.servlet.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebListener
public class WebAppListener implements ServletContextListener {
	
    private static final Logger LOGGER = LoggerFactory.getLogger(WebAppListener.class);

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		LOGGER.info("WebAppListener通过注解实现的监听器开始初始化...");
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		 LOGGER.info("WebAppListener通过注解实现的监听器已销毁...");
	}

}
