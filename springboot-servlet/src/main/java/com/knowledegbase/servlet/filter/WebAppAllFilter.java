package com.knowledegbase.servlet.filter;

import java.io.IOException;
import java.util.Date;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;

@WebFilter(filterName="WebAppAllFilter",urlPatterns= {"/*"})
@Order(1)
public class WebAppAllFilter implements Filter {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(WebAppAllFilter.class);

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		LOGGER.info("WebAppAllFilter - {}初始化注解实现的过滤器...",new Date());
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest)request;
		HttpServletResponse resp = (HttpServletResponse)response;
		LOGGER.info("WebAppAllFilter - Request URL:{}",req.getRequestURL().toString());
		LOGGER.info("WebAppAllFilter - Request port:{}",req.getServerPort());
		LOGGER.info("WebAppAllFilter - Request method:{}",req.getMethod());
		resp.setHeader("Current-Path", req.getServletPath());
		resp.setHeader("My-name", "KnowledgeBase");
		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
		LOGGER.info("WebAppAllFilter - 注解实现的过滤器已销毁...");
	}

}
