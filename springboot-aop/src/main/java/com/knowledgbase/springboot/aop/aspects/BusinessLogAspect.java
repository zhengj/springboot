package com.knowledgbase.springboot.aop.aspects;

import java.lang.reflect.Method;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import com.knowledgbase.springboot.aop.annotation.BusinessLogAnnotation;

@Component
@Aspect
@Order(-2)
public class BusinessLogAspect {
	public static final Logger LOGGER = LoggerFactory.getLogger(BusinessLogAspect.class);
	
	@Pointcut(value="@annotation(com.knowledgbase.springboot.aop.annotation.BusinessLogAnnotation)")
	public void pointCut() {
	}
	
	@Before("pointCut()")
	public void doBefore(JoinPoint joinPoint) throws NoSuchMethodException, SecurityException {
		Signature sign = joinPoint.getSignature();
		MethodSignature msign = null;
		if(!(sign instanceof MethodSignature)) {
			LOGGER.error("该注解只能用于方法");
		}
		msign = (MethodSignature) sign;
		Object target = joinPoint.getTarget();
		Method currMethode = target.getClass().getMethod(msign.getName(), msign.getParameterTypes());
		BusinessLogAnnotation annotation = currMethode.getAnnotation(BusinessLogAnnotation.class);
		String bussinessName = annotation.value();
		LOGGER.info("进入{}方法",bussinessName);
	}
	
}
