package com.knowledgbase.springboot.aop.controller;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.knowledgbase.springboot.aop.annotation.BusinessLogAnnotation;

@RestController
public class AopController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AopController.class);
	
	
	@RequestMapping("/")
	public Object index(HttpServletRequest req) {
		Enumeration<String> parameterNames = req.getParameterNames();
		List<String> params = new ArrayList<String>();
		while(parameterNames.hasMoreElements()) {
			String paramName = parameterNames.nextElement();
			String param = paramName +":"+req.getParameter(paramName);
			LOGGER.info(param);
			params.add(param);
		}
		return params;
	}
	
	@RequestMapping("/log")
	@BusinessLogAnnotation("查看log")
	public Object log(HttpServletRequest req) {
		return req.getRequestURI();
	}
	
	
}
