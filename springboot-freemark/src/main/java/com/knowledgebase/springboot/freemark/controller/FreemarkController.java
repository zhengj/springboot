package com.knowledgebase.springboot.freemark.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class FreemarkController {

	@RequestMapping("/")
	public String index() {
		return "index";
	}
	
	@RequestMapping("/toUpload")
	public String toUpload() {
		return "upload";
	}
	
	@RequestMapping("/toFormdata")
	public String toFormdata() {
		return "formdata";
	}
	
	@RequestMapping("/upload")
	@ResponseBody
	public String upload(@RequestParam("file") MultipartFile files[],String name,HttpServletRequest req ) {
					
		if(files!=null) {
			for(MultipartFile file : files) {
				System.out.println(file.getOriginalFilename());
			}
		}
		System.out.println(req.getParameter("name"));
		return name;
	}
	
	
	@RequestMapping("/formdata")
	@ResponseBody
	public String formdata(@RequestParam("file") MultipartFile files[], String name,HttpServletRequest request) {
		if(files != null){
			for(MultipartFile file : files){
				System.out.println(file.getOriginalFilename());
			}
		}
		System.out.println(request.getParameter("name"));
		return name;
	}
	
	
}
